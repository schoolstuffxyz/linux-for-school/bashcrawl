#!/bin/bash

shopt -s expand_aliases
trap '' 2

lang=$(locale | grep LANG | cut -d "=" -f 2 | cut -d "." -f 1 | cut -d "_" -f 1)
source_dir="/usr/share/bashcrawl"
# source_dir="/home/azure/.local/source/schoolstuff/bashcrawl-de"
run_dir="$HOME/.cache/bashcrawl"

save_dir="$run_dir/save" && mkdir -p "$save_dir"
alias_file="$save_dir/aliases.txt" && touch "$alias_file"
health_file="$save_dir/health.txt" && touch "$health_file"
inventory_file="$save_dir/inventory.txt" && touch "$inventory_file"
history_file="$save_dir/history.txt" && touch "$history_file"
room_file="$save_dir/room.txt" && touch "$room_file"
lang_file="$save_dir/lang.txt" && touch "$lang_file"

line="--------------------------------------------------------------------------------\n"

# parsing options
case "$1" in
    --binder) source_dir="$HOME";;
    '') echo "running bashcrawl" ;;
    *) 
        echo "bashcrawl usage: ./bashcrawl [--help][--binder]" >&2
        echo "" >&2
        echo "--help        :   to display this help message" >&2
        echo "--binder      :   for using on a binder instance" >&2
        exit 1;;
esac

# Thanks to manatwork for this great function
box_out() {
    local s=("$@") b w
    for l in "${s[@]}"; do
        ((w < ${#l})) && {
            b="$l"
            w="${#l}"
        }
    done
    tput setaf 3
    echo " ---${b//?/-}---
|     ${b//?/ }   |"
    for l in "${s[@]}"; do
        printf '|   %s%*s%s   |\n' "$(tput setaf 4)" "-$w" "$l" "$(tput setaf 3)"
    done
    echo "|   ${b//?/ }   |
 -  --${b//?/-}---"
    tput sgr 0
}

do_cat() {
    if ! [[ -f $args ]]; then
        case $lang in
            de*) echo -e "Es gibt kein \"$args\", versuche es noch einmal.\n" ;;
            *) echo -e "There is no \"$args\", try again.\n" ;;
        esac
        return
    fi
    if [[ -x $args ]]; then
        case $lang in
            de*) echo -e "Hmm, das hat nicht funktioniert.\nVersuch weiterzugehen, vielleicht kannst\ndu später zu diesem $args zurückkommen!\n" ;;
            *) echo -e "Hmm, that didn't work.\nTry moving along, maybe you\ncan come back to this $args later!\n" ;;
        esac
        return
    fi
    cat "$args"
}

do_cd() {
    if ! [[ -d $args ]]; then
        case $lang in
            de*) echo -e "Diesen Ort gibt es nicht, probiere es noch einmal.\n" ;;
            *) echo -e "There is no such place, try again.\n" ;;
        esac
        return
    fi
    cd "$args" || return
    room="$(pwd | rev | cut -d "/" -f 1 | rev)"
    case $lang in
        de*) echo -e "Du hast einen Raum betreten:\n- $room\n\nWas möchtest du nun tun?\n" ;;
        *) echo -e "You entered a room:\n- $room\n\nWhat do you want to do now?\n" ;;
    esac
    pwd >"$room_file"
}

get_history() {
    grep "$1" "$history_file" >/dev/null || return
    echo -e "$1\t$2"
}

parse_commands() {
    # shellcheck disable=SC1090
    case $command in
        "cat") do_cat ;;
        "ls")
            case $lang in
                de*) echo -e "Du hast die folgenden Dinge gefunden:\n" ;;
                *) echo -e "You found the following things:\n" ;;
            esac
            # shellcheck disable=SC2012
            # shellcheck disable=SC2086
            printf " ---------\n|\n%s\n|\n ---------\n" "$(ls $args | sed 's/^/|   /')"
            case $lang in
                de*) echo -e "\nWas willst du als nächstes tun?\n" ;;
                *) echo -e "\nWhat would you like to do next?\n" ;;
            esac
            ;;
        "cd") do_cd ;;
        "export")
            eval "export $args"
            eval "echo $HP >> $health_file"
            eval "echo $I >> $inventory_file"
            ;;
        "echo") eval "echo $args" ;;
        "pwd")
            case $lang in
                de*) echo -e "Du bist hier:\n" ;;
                *) echo -e "You are here:\n" ;;
            esac
            pwd | sed -e 's/^.*bashcrawl//'
            echo ""
            ;;
        "ln") eval "ln $args" ;;
        "tree")
            case $lang in
                de*) echo -e "Das ist die Karte der Gemäuer:\n" ;;
                *) echo -e "This is the map of the catacombs:\n" ;;
            esac
            # shellcheck disable=SC2086
            printf "\n\n ---------\n|\n%s\n|\n ---------\n\n" "$(tree $args | sed -e '/^$/d' -e '/^[0-9]*\s/d' -e 's/^/|   /')"
            case $lang in
                de*) echo -e "\nWas willst du als nächstes tun?\n" ;;
                *) echo -e "\nWhat would you like to do next?\n" ;;
            esac
            ;;
        ./*) eval "$command" || main ;;
        "alias")
            echo "$input" >>"$alias_file"
            source "$alias_file"
            ;;
        "let")
            eval let "$args"
            eval "echo $HP >> $health_file"
            ;;
        "help" | "hilfe")
            case $lang in
                de*)
                    echo -e "quit\t\t\t\tVerlasse das Spiel.\n"
                    echo -e "cat\t<schriftrolle>\t\tLese eine Schriftrolle (Dokument)."
                    echo -e "ls\t<evtl. Raum>\t\tListe alle Dinge (Dateien) im Raum auf."
                    get_history "cd" "<Raum>\t\t\tBewege dich in einen anderen Raum."
                    get_history "alias" "<ls=\"ls -F\">\t\tBenenne einen Befehl um."
                    get_history "export" "<HP=10>\t\t\tExportiere eine Variable."
                    get_history "echo" "<\$HP>\t\t\tSage etwas und zeige den Inhalt einer Variable."
                    get_history "let" "<HP=HP-5>\t\tVerändere eine Variable (let kann rechnen)."
                    get_history "pwd" "\t\t\tZeige den aktuellen Standort an."
                    get_history "ln" "<wohin?> <portal>\tVerlinke einen Raum mit einem Portal."
                    get_history "tree" "\t\t\tZeige die Karte der Gemäuer an."
                    echo -e "\nhelp\t\t\t\tZeige diese Hilfe-Nachricht an."
                    echo -e "\nInfo:\tDie \"Tab\"-Taste kann angefangene Wörter\n\tautomatisch vervollständigen.\n"
                    ;;
                *)
                    echo -e "quit\t\tQuit the game.\n"
                    echo -e "cat\t\tread a document"
                    echo -e "ls\t\tlist all things in a room"
                    get_history "cd" "move to another room"
                    get_history "alias" "rename a command"
                    get_history "export" "export a variable"
                    get_history "echo" "say something or show a variable's content"
                    get_history "let" "change a variable (let may calculate)"
                    get_history "pwd" "show your current location"
                    get_history "ln" "link a room with a portal"
                    get_history "tree" "show the map of the catacombs"
                    echo -e "\nhelp\t\tShow this help message."
                    echo -e "\nInfo:\t\tThe \"tab\" key automatically completes\n\t\tthe words you are writing."
                    ;;
            esac
            ;;
        "quit")
            trap 2
            clear
            exit 0
            ;;
        *)
            case $lang in
                de*) echo -e "Ich habe diesen Befehl nicht erkannt.\n" ;;
                *) echo -e "I did not recognize this command.\n" ;;
            esac
            command="help" && parse_commands
            ;;
    esac
}

start() {
    clear
    case $lang in
        de*) echo -e "\nWähle deine Sprache (Gib die Nummer ein und drücke <Enter>).\n" ;;
        *) echo -e "\nChoose your language (Enter the number and press <Enter>).\n" ;;
    esac

    echo -e "\n(1) English\n(2) Deutsch\n"
    echo -e "$line"
    read -rep "> " lang
    case "$lang" in
        "quit")
            trap 2
            exit 0
            ;;
        1* | eng* | Eng*) lang=en ;;
        2* | ger* | Ger* | deu* | Deu*) lang=de ;;
        *) lang=english ;;
    esac

    clear
    case $lang in
        de*)
            source_dir="$source_dir/eingang"
            run_dir="$run_dir/eingang"
            ;;
        *)
            source_dir="$source_dir/entrance"
            run_dir="$run_dir/entrance"
            ;;
    esac

    if [ -d "$run_dir" ]; then
        load_save_state
        return
    else
        new_game
    fi
}

load_save_state() {
    case $lang in
        de*) echo -e "\nDu hast das Spiel schon einmal gespielt.\n\nMöchtest du dort weiterspielen,\nwo du aufgehört hast?\n\n(Schreibe J[a] oder n[ein])\n\n$line" ;;
        *) echo -e "\nYou already played this game.\n\nWould you like to continure,\nwhere you left off?\n\n(Write Y[es] or n[o])\n\n$line" ;;
    esac
    read -rep "> " yn
    case $yn in
        "quit")
            trap 2
            exit 0
            ;;
        n* | N*) new_game ;;
        *) load_game ;;
    esac
}

new_game() {
    [ -d "$run_dir" ] && rm -rf "$run_dir"
    true >"$alias_file"
    true >"$health_file"
    true >"$inventory_file"
    true >"$history_file"
    true >"$room_file"
    cp -r "$source_dir" "$run_dir"
    export HP=10
    history -c

    cd "$run_dir" || return

    clear
    case $lang in
        de*)
            echo -e "\nDu hast ein uraltes Gemäuer betreten.\n\nVor dir liegt eine alte Schriftrolle.\n\nUm sie zu lesen, tippe:\n\tcat schriftrolle\n\nund drücke die <Enter>-Taste.\n"
            echo -e "\nMit dem \"hilfe\" oder \"help\" Befehl kannst du dir noch einmal anschauen,\nwas die gelernten Befehle können."
            ;;
        *)
            echo -e "\nYou have entered some ancient catacombs.\n\nIn front of you lies an old scroll.\n\nTo read it, type:\n\tcat scroll\n\nand press the <Enter> key.\n"
            echo -e "\nWith the \"help\" command you may look,\nwhat the already learnt commands do."
            ;;
    esac

    main
}

# shellcheck disable=SC1090
load_game() {
    clear
    case $lang in
        de*) echo -e "Lade den Spielstand und betrete Gemäuer ..." ;;
        *) echo -e "Loading save state and entering catacombs ..." ;;
    esac
    sleep 2
    [[ -f "$room_file" ]] && run_dir="$(cat "$room_file")"
    [[ -f "$alias_file" ]] && source "$alias_file"
    [[ -f "$health_file" ]] && HP=$(cat "$health_file") && export HP
    [[ -f "$inventory_file" ]] && I=$(cat "$inventory_file") && export I
    [[ -f "$history_file" ]] && history -r "$history_file"

    cd "$run_dir" || return

    clear
    case $lang in
        de*) echo -e "\nDu hast die Gemäuer wieder betreten.\n\nMit dem \"hilfe\" oder \"help\" Befehl kannst du dir nich einmal anschauen,\nwas die gelernten Befehle können.\n" ;;
        *) echo -e "\nYou again have entered the catacombs.\n\nWith the \"help\" command you may look,\nwhat the already learnt commands do.\n" ;;
    esac

    main
}

main() {
    pwd >"$room_file"
    echo -e "$line"
    case $lang in
        de*) echo -e "Nutze den Befehl 'hilfe', um dir die gelernten Befehle noch einmal anzuschauen.\n" ;;
        *) echo -e "Use the command 'help' to look at the commands you already learnt.\n" ;;
    esac
    echo -e "$line"
    read -rep "$ " input
    history -s "$input"
    history >"$history_file"
    command=$(echo "$input" | cut -d " " -f 1)
    args=$(echo "$input" | cut -d " " -f 2-)
    [[ $args = "$command" ]] && args=""
    clear
    [[ $input ]] && echo -e "\n$ $input\n"

    parse_commands && main
}

tty -s
# shellcheck disable=SC2181
if [ "0" = "$?" ]; then
    echo "Terminal attached, you can print data as there might be a user viewing it."
else
    echo "No terminal attached, there might not be a reason to print everything."
    [[ $(command -v st) ]] && st -e bashcrawl && exit 0
    [[ $(command -v kitty) ]] && kitty -e bashcrawl && exit 0
    [[ $(command -v xterm) ]] && xterm -e bashcrawl && exit 0
    [[ $(command -v gnome-terminal) ]] && gnome-terminal -e bashcrawl && exit 0
    exit 0
fi

start
